import React, { Component } from "react";
import ipfs from "./ipfs";
import { saveAs } from "file-saver";

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // account: '',
      // productCount: 0,
      // products: [],
      // loading: true,
      buffer: null,
      ipfsHash: "",
    };

    // this.createProduct = this.createProduct.bind(this)
    // this.purchaseProduct = this.purchaseProduct.bind(this)

    this.captureFile = this.captureFile.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  createProduct(name, price, ipfsHash) {
    this.setState({ loading: true });
    this.state.marketplace.methods
      .createProduct(name, price, ipfsHash)
      .send({ from: this.state.account })
      .once("receipt", (receipt) => {
        this.setState({ loading: false });
      });
  }

  purchaseProduct(id, price, ipfsHash) {
    this.setState({ loading: true });
    this.state.marketplace.methods
      .purchaseProduct(id)
      .send({ from: this.state.account, value: price, value2: ipfsHash })
      .once("receipt", (receipt) => {
        this.setState({ loading: false });
      });
  }

  // captureFile(event) {
  //   event.preventDefault()
  //   const file = event.target.files[0]
  //   const reader = new window.FileReader()
  //   reader.readAsArrayBuffer(file)
  //   reader.onloadend = () => {
  //     this.setState({ buffer: Buffer(reader.result)})
  //     console.log('buffer', this.state.buffer)
  //   }
  // }

  // SubmitFile(event) {
  //   event.preventDefault()
  //   ipfs.files.add(this.state.buffer, (error, result) => {
  //     if(error){
  //       console.error(error)
  //       return
  //     }
  //     this.setState({ ipfsHash: result[0].hash })
  //     console.log('ipfsHash', this.state.ipfsHash)
  //   })
  // }

  captureFile(event) {
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      this.setState({ buffer: Buffer(reader.result) });
      console.log("buffer", this.state.buffer);
    };
  }

  onSubmit(event) {
    event.preventDefault();
    console.log("btn-is clicked");
    ipfs.files.add(this.state.buffer, (error, result) => {
      if (error) {
        console.error(error);
        return;
      }
      this.setState({ ipfsHash: result[0].hash });
      console.log("ipfsHash", this.state.ipfsHash);
      
    });
  }

  onDownload = async (fileLink) => {
    // const image = `https://ipfs.io/ipfs/QmVHA9NCSU8aL1E2ahWcLrsDh7RFkD8e9nH6nkdhYL2gn3`;
    const image = `${fileLink}`;
    console.log(fileLink);
    console.log("download start");
    let fileBtn = document.getElementById("file_picker_btn");
    const downloadResult = await fetch(image);
    const blob = await downloadResult.blob();
    saveAs(blob );
  };

  onFormSubmit = (event) => {
    event.preventDefault();
    const name = this.productName.value;
    const price = window.web3.utils.toWei(
      this.productPrice.value.toString(),
      "Ether"
    );
    // const ipfsHash = this.productHash.value
    this.props.createProduct(name, price, this.state.ipfsHash);
  };

  render() {
    return (
      
      <div id="content">
        <h3>Add Product</h3>
        <input
          id="productName"
          type="text"
          ref={(input) => {
            this.productName = input;
          }}
          className="form-control"
          placeholder="Product Name"
          required
        />
        <br></br>
        <input
          id="productPrice"
          type="text"
          ref={(input) => {
            this.productPrice = input;
          }}
          className="form-control"
          placeholder="Product Price"
          required
        />
        <br></br>
        <input
          id="productIpfsHash"
          type="file"
          onChange={this.captureFile}
          ref={(ipfsHash) => {
            this.productHash = ipfsHash;
          }}
          required
        />
        <button onClick={this.onSubmit}>Upload file</button>
        <br></br>
        <br></br>
        
        <button
          type="button"
          onClick={this.onFormSubmit}
          className="btn btn-primary"
        >
          Add Product
        </button>
        <p>&nbsp;</p>
        <h3>Buy Product</h3>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Owner</th>
              <th scope="col">Data</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody id="productList">
            {this.props.products.map((product, key) => {
              return (
                <tr key={key}>
                  <th scope="row">{product.id.toString()}</th>
                  <td>{product.name}</td>
                  <td>
                    {window.web3.utils.fromWei(
                      product.price.toString(),
                      "Ether"
                    )}{" "}
                    ETH
                  </td>
                  <td>{product.owner}</td>
                  <td>
                  {product.purchased ? 
                    <button
                      onClick={() =>
                        this.onDownload(
                          `https://ipfs.io/ipfs/${product.ipfsHash}`
                        )
                      }
                    >{`https://ipfs.io/ipfs/${product.ipfsHash}`}</button>
                    : null}
                  </td>

                  <td>
                    {!product.purchased ? 
                      <button
                        name={product.id}
                        value={product.price}
                        value2={product.ipfsHash}
                        onClick={(event) => {
                          this.props.purchaseProduct(
                            event.target.name,
                            event.target.value,
                            event.target.ipfsHash
                          )
                        }}
                      >
                        Buy
                      </button>
                     : null}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Main;
